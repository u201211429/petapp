class SessionsController < Devise::SessionsController
  # https://github.com/plataformatec/devise/blob/master/app/controllers/devise/sessions_controller.rb

  # POST /resource/sign_in
  # Resets the authentication token each time! Won't allow you to login on two devices
  # at the same time (so does logout).
  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)

    current_user.update authentication_token: nil

    respond_to do |format|
      format.html {
        super
      }
      format.json {
        render :json => {
          :user_id => current_user.id,
          :user_email => current_user.email,
          :status => :ok,
          :authentication_token => current_user.authentication_token
        }
      }
    end
  end

  # DELETE /resource/sign_out
  def destroy
    current_user.update authentication_token: nil
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    puts 'nigger'

    respond_to do |format|
      format.html {
        super
      }
      format.all {
        render :json => {
          :nigger => 'faggot'
        }
      }
    end
  end
end