class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new 
    if user.has_role? :admin
      can :manage, :all
    elsif user.has_role? :person
      can :read, :all
      can [:update,:destroy], Profile, :user_id => user.id  
      can :create, Profile, :user_id => user.id
      can [:update,:destroy], Pet, :user_id => user.id  
      can :create, Pet, :user_id => user.id
      can [:update,:destroy], Photo, :pet_id => user.pets.ids
      can :create, Photo
    else
      can :read, :all
    end
  end
end
    