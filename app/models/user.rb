class User < ActiveRecord::Base
  acts_as_token_authenticatable
  
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  ratyrate_rater
  has_one :profile
  has_many :pets

  after_create :assign_default_role

  
  def assign_default_role
    add_role(:person)
  end  
end
