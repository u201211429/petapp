class Photo < ActiveRecord::Base
  belongs_to :pet
  validates :pet, presence: true
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "125x125>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/


  resourcify

end
