json.array!(@photos) do |photo|
  json.extract! photo, :id, :comment, :is_avatar, :pet_id
  json.url photo_url(photo, format: :json)
end
