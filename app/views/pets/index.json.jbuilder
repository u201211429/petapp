json.array!(@pets) do |pet|
  json.extract! pet, :id, :name, :breed, :pedigree, :born_at, :about, :gender, :user_id
  json.url pet_url(pet, format: :json)
end
