json.profile @profile, :id, :avatar, :first_name, :last_name, :email, :lat, :long, :user_id, :created_at, :updated_at

json.pet_count @profile.user.pets.count

json.pets @profile.user.pets do |pet|
  json.extract! pet, :id, :name, :breed, :pedigree, :born_at, :about, :gender, :user_id, :created_at, :updated_at
  json.photos pet.photos do |photo|
    json.picture_url photo.picture.url
  end
end