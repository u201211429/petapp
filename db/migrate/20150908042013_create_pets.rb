class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :breed
      t.boolean :pedigree
      t.date :born_at
      t.text :about
      t.string :gender
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
