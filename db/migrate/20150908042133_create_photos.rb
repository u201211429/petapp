class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.text :comment
      t.boolean :is_avatar
      t.references :pet, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
